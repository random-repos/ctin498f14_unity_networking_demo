﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;


#if !UNITY_ANDROID 
//dummy implementations of TouchScreenKeyboard
//The TouchScreenKeyboard class is only available on Android/iOs build and I want to be able to compile for desktop as well
public class TouchScreenKeyboard
{
    public static TouchScreenKeyboard Open(string arg1, TouchScreenKeyboardType arg2)
    {
        var r = new TouchScreenKeyboard();
        r.text = arg1;
        return r;
    }

    public bool done = true;
    public string text;
}

public enum TouchScreenKeyboardType
{
    NumbersAndPunctuation
}
#endif

[RequireComponent (typeof (NetworkView))]
public class ClientBehaviour : MonoBehaviour {
    public string IP = "";
    public int port = 12321;

    NetworkView mView;

    TouchScreenKeyboard keyboard = null; //the virtual keyboard (mobile only)

    //network status properties
    public bool Connecting{get;private set;}
    public bool Connected{get;private set;}

    //this code has been moved to GuiTextDisplay but I left it here as an example of how to directly modify this code if you don't like the SendNetworkMessage approach
    /*
    string lastMessageReceived = ""; //this is the last message we recieved from RecieveNetworkMessage
    void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        style.fontSize = 50;
        style.alignment = TextAnchor.MiddleCenter;
        style.normal.textColor = Color.white;
        style.wordWrap = true;
        GUI.Label(new Rect(0, 0, Screen.width, Screen.height),lastMessageReceived, style);
    }*/

    //this function is called by the NetworkView whenever the server or another client sends a message
    [RPC]
    void RecieveNetworkMessage(string aMsg)
    {
        //lastMessageReceived = aMsg; see comment block above

        //NOTE this will only call 'OnNetworkMessage' on scripts that are attached to 'this.gameObject'
        SendMessage("OnNetworkMessage", aMsg, SendMessageOptions.DontRequireReceiver);
    }
   
    //helper function for sending messages to the server and other clients on the netwrok
    public void SendNetworkMessage(string aMsg)
    {
        mView.RPC("RecieveNetworkMessage", RPCMode.Others, new object[]{aMsg});
    }

    //use to connect to network
    void ClientConnect()
    {
        DebugConsole.Log("trying to connect to server " + IP + " on port " + port);
        Connecting = true;
        var error = Network.Connect(IP, port); //try and connect
        if (error != NetworkConnectionError.NoError)
        {
            Connecting = false;
            DebugConsole.Log("Connection error " + error.ToString());
        }
    }

    //use to initialize network connection (including obtaining IP address if none was specified)
    void InitializeIpEntry()
    {
        DebugConsole.Log("getting ip via touchscreen keyboard");
        keyboard = TouchScreenKeyboard.Open(IP,TouchScreenKeyboardType.NumbersAndPunctuation);
    }

    void Start () {
        mView = gameObject.GetComponent<NetworkView>();
        //we uses RequireComponent so the NetworkView is guaranteed to exist, but alternatively, you could create it automatically if it does not exists. Something I quite like doing in my own code
        //if(mView == null) //if there is no network view then create one
            //mView = gameObject.AddComponent<NetworkView> ();
        
        //disable state synchronization for this object. 
        //in this example, we only use RPC
        mView.stateSynchronization = NetworkStateSynchronization.Off; 

        //initialize the client
        //if you didn't want to do this, you could hard code IP and call ClientConnect directly here
        InitializeIpEntry();
    }

    void Update () {

        //handle keyboard for IP entry (mobile only)
        if(keyboard != null && keyboard.done)
        {
            IP = keyboard.text;
            DebugConsole.Log("IP set to " + IP);
            keyboard = null;
            ClientConnect();
        }

        //EXAMPLE, send a mesasge whenever we register a touch
        if (Input.GetMouseButtonDown(0))
        {
            SendNetworkMessage("touch registered on client");
        }
    }

    //------------------
    //network related callbacks
    //similar to things like OnCollisionEnter etc..
    //------------------
    //this is called on the CLIENT when it fails to connect to the server
    //sometimes the connect function wont fail right away and instead will trigger this callback a while later
    public void OnFailedToConnect(NetworkConnectionError error) {
        DebugConsole.Log("Could not connect to server: " + error + " ...trying again");
        InitializeIpEntry(); //try to reconnect, this will allow you do launch the client befoer you launch the server among other things
    }
    
    //this is called on the CLIENT when it succesfully connects to the server
    public void OnConnectedToServer()
    {
        Connecting = false;
        Connected = true;
        DebugConsole.Log("Connected to server");
        SendNetworkMessage("Hi, I just connected to your wonderful server");
    }
    
    //this is called on the CLIENT when it succesfully connects to the server
    public void OnDisconnectedFromServer(NetworkDisconnection info) 
    {
        Connected = false;
        DebugConsole.Log("Disconnected from server");
        InitializeIpEntry(); //try to reconnect
    }
}
