﻿using UnityEngine;
using System.Collections;


public class GuiTextDisplay : MonoBehaviour {
    string lastMessageReceived = ""; //this is the last message we recieved from RecieveNetworkMessage


    void OnNetworkMessage(string aMsg)
    {
        DebugConsole.Log("OnNetworkMessage");
        lastMessageReceived = aMsg;
    }

    void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        style.fontSize = 50;
        style.alignment = TextAnchor.MiddleCenter;
        style.normal.textColor = Color.white;
        style.wordWrap = true;
        GUI.Label(new Rect(0, 0, Screen.width, Screen.height),lastMessageReceived, style);
    }

}
