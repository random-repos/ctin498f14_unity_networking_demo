﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[RequireComponent (typeof (NetworkView))]
public class ServerBehaviour : MonoBehaviour {
    public int port = 12321;

    NetworkView mView;
   
    //this function is called by the NetworkView whenever a client sends a message
    [RPC]
    void RecieveNetworkMessage(string aMsg)
    {

        //NOTE this will only call 'OnNetworkMessage' on scripts that are attached to 'this.gameObject'
        SendMessage("OnNetworkMessage", aMsg, SendMessageOptions.DontRequireReceiver);
    }

    //helper function for sending messages to other clients on the netwrok
    void SendNetworkMessage(string aMsg)
    {
        mView.RPC("RecieveNetworkMessage", RPCMode.All, new object[]{aMsg});
    }

	void Start () {

        //create a NetworkView 
        mView = gameObject.GetComponent<NetworkView>();
        //disable state synchronization for this object as we only need RPC in this example
        mView.stateSynchronization = NetworkStateSynchronization.Off; 

        DebugConsole.Log("initializing server");

        //initialize the server
        Network.InitializeServer(2, port, false);// !Network.HavePublicAddress());
	}
	

	void Update () {
        //EXAMPLE
        //send the message type when the user hits return (note, you'll have to 'lose focus' from the text entry box for the return key to be registered)
        if (Input.GetKeyDown(KeyCode.Return))
        {
            SendNetworkMessage(inprocesstext);
            inprocesstext = "";
        }
	}

    //EXAMPLE, sending messages that the user types in the screen
    string inprocesstext="";
    void OnGUI()
    {
        GUIStyle style = new GUIStyle("TextArea");
        style.fontSize = 30;
        style.alignment = TextAnchor.MiddleCenter;
        style.normal.textColor = Color.white;
        style.wordWrap = true;
        inprocesstext = GUI.TextArea(new Rect(Screen.width/2, 0, Screen.width/2, Screen.height/2), inprocesstext, style);
    }

    //------------------
    //network related callbacks
    //similar to things like OnCollisionEnter etc..
    //------------------

    //this is called on the SERVER when a client connects to the server
    public void OnPlayerConnected(NetworkPlayer aPlayer)
    {
        DebugConsole.Log(aPlayer.ipAddress + " connected");
        SendNetworkMessage("Hi, welcome to my server");
    }

    //this is called on the SERVER when a client disconnects from the server
    public void OnPlayerDisconnected(NetworkPlayer aPlayer) 
    {
        DebugConsole.Log(aPlayer.ipAddress + " disconnected :(");
    }
}
